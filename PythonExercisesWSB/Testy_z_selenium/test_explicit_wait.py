from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import pytest
from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver

@pytest.fixture(scope="session")
def chromedriver(request):
    driver = webdriver.Chrome(executable_path="webdriver\\chromedriver.exe")

    def chromedriver_teardown():
        driver.close()

    request.addfinalizer(chromedriver_teardown)
    return driver


@pytest.fixture(scope="session")
def prestashop_driver(chromedriver):
    """
    :type chromedriver: WebDriver
    """
    chromedriver.get("http://demo.prestashop.com/")
    chromedriver.switch_to.frame(0)
    return chromedriver


def test_main_page(prestashop_driver):
    try:
        element = WebDriverWait(prestashop_driver, 10).until(
            EC.presence_of_element_located((By.XPATH, "//span[contains(.,'Sign in')]")))
        assert "Sign in" in element.text
    except Exception as e:
        print(e)


