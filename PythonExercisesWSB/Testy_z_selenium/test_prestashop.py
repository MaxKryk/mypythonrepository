import time
import json
import pytest
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.remote.webdriver import WebDriver


EMAIL = "email@wsb.wroclaw.pl"
PASSWORD = "email123"


@pytest.fixture(scope="session")
def chromedriver(request):
    driver = webdriver.Chrome(executable_path="webdriver\\chromedriver.exe")

    def chromedriver_teardown():
        driver.close()

    request.addfinalizer(chromedriver_teardown)
    return driver


@pytest.fixture(scope="session")
def prestashop_driver(chromedriver):
    """
    :type chromedriver: WebDriver
    """
    chromedriver.get("http://demo.prestashop.com/")
    chromedriver.switch_to.frame(0)
    return chromedriver


def test_main_page(prestashop_driver):
    assert "PrestaShop Demo" in prestashop_driver.title


def test_user_login(prestashop_driver):
    """
    :param prestashop_driver:
    :type prestashop_driver: WebDriver
    """
    driver = prestashop_driver

    # Tutaj wprowadz swoj kod
    assert False


def test_subpage_navigate_to_art_category(prestashop_driver):
    """
    :param prestashop_driver:
    :type prestashop_driver: WebDriver
    """
    driver = prestashop_driver

    # Tutaj wprowadz swoj kod
    assert False


def test_select_product_from_produts_list(prestashop_driver):
    """
    :param prestashop_driver:
    :type prestashop_driver: WebDriver
    """
    driver = prestashop_driver

    # Tutaj wprowadz swoj kod
    assert False


def test_product_order(prestashop_driver):
    """
    :param prestashop_driver:
    :type prestashop_driver: WebDriver
    """
    driver = prestashop_driver

    # Tutaj wprowadz swoj kod
    assert False


def test_cart_edit_quantity(prestashop_driver):
    """
    :param prestashop_driver:
    :type prestashop_driver: WebDriver
    """
    driver = prestashop_driver

    # Tutaj wprowadz swoj kod
    assert False


def test_cart_finalize_order(prestashop_driver):
    """
    :param prestashop_driver:
    :type prestashop_driver: WebDriver
    """
    driver = prestashop_driver

    # Tutaj wprowadz swoj kod
    assert False
