#1 funkcja z dwoma argumentami na wejsciu. jesli sa wieksze od zera, sumujemy je, jesli mniejsze
#odejmujemy, jesli sa rozne, zwracamy 0
def twoArgs(a, b):
    if (a>0) and (b>0):
        return a+b
    elif (a<0) and (b<0):
        return a-b
    elif (a!=0):
        return 0;

print(twoArgs(4,5))