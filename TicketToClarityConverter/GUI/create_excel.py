from GUI.logging import logger_setup
import logging
from openpyxl import Workbook
from colorama import Style
from openpyxl import Workbook
from openpyxl import load_workbook
from GUI.logging import logger_setup
from openpyxl.styles import Font, Color, Alignment, Border, Side, colors
from openpyxl.styles import PatternFill, colors
from openpyxl.worksheet.datavalidation import DataValidation
from openpyxl import formula


def create_excel():
    logger_setup()
    log = logging.getLogger("create_excel_if_not_exist()")
    workbook = Workbook()
    log.info("Creating sheets read from")
    read_from_sheet = workbook.create_sheet("Read_from")
    log.info("Creating sheets write to")
    write_to_sheet = workbook.create_sheet("Write_to")
    log.info("Creating sheets data")
    data_sheet = workbook.create_sheet("Data")
    log.info("workbook spreadsheets: ")
    log.info(workbook.get_sheet_names)

    # fill the columns with the elements from the list
    read_from_sheet["A1"] = "hello"
    read_from_sheet["B1"] = "hello"
    read_from_sheet["C1"] = "hello"
    read_from_sheet["D1"] = "hello"
    read_from_sheet["E1"] = "hello"

    for cell in read_from_sheet.iter_cols(min_row=2,
                                          max_row=2,
                                          min_col=2,
                                          values_only=True):
        cell = "xxxx"
    log.info("Create a list and append colnames")
    colnames = []
    colnames.append('activity')
    colnames.append('date')
    colnames.append('ticket_no')
    colnames.append('solver')
    colnames.append('ticket_title')

    colnames.append('external')
    colnames.append('way_of_solution')
    colnames.append('time')
    colnames.append('cluster')
    colnames.append('day')
    colnames.append('log')
    colnames.append('ticket_type')
    colnames.append('clarity_entry')
    colnames.append('other_activity')
    colnames.append('week number')

    log.info("Take the list colnames and put the elements into the first row")
    for x in range(1, len(colnames) + 1):
        read_from_sheet.cell(row=1, column=x).value = colnames[x - 1]  # fill the column names
        read_from_sheet.cell(row=1, column=x).font = Font(bold=True)  # make them bold



    #PREPARING READ FROM SHEET
    # FILL THE DATA
    data_sheet["S1"] = "ERSTANA"
    data_sheet["S2"] = "Y"
    data_sheet["S3"] = "N"

    # DATA VALIDATION DATE
    dateValidation = DataValidation(type="date")
    dateValidation.add(read_from_sheet["B2"])
    dateValidation.error = 'Your entry does not have the correct date format'
    dateValidation.errorTitle = 'Invalid Entry'
    read_from_sheet.add_data_validation(dateValidation)
    ##DATA VALIDATION ACTIVITY


    activity_validation = DataValidation(type="list", formula1='"Solved, EA, MIM, EIN, KT, DISPATCH"',
                                         allow_blank=True)
    activity_validation.error = 'Your entry is not in the list'
    activity_validation.errorTitle = 'Invalid Entry'
    activity_validation.prompt = 'Please select from the list'
    activity_validation.promptTitle = 'List Selection'
    read_from_sheet.add_data_validation(activity_validation)
    c1 = read_from_sheet["A2"]
    c1.value = "Select"
    activity_validation.add(c1)

    ##DATA EXTERNAL
    external_validation= DataValidation(type="list", formula1='"PRU,\
        INU,\
        WAU, RTU, Solved"', allow_blank=True)
    external_validation.error = 'Your entry is not in the list'
    external_validation.errorTitle = 'Invalid Entry'
    external_validation.prompt = 'Please select from the list'
    external_validation.promptTitle = 'List Selection'
    read_from_sheet.add_data_validation(external_validation)
    c4 = read_from_sheet["F2"]
    c4.value = "Select"
    external_validation.add(c4)
    ##DATA VALIDATION TICKET WOS
    ticket_wos_validation = DataValidation(type="list", formula1='"WAD/RF,\
        WAD/BRF,\
        WAD/TF,\
        WAD/SF,\
        WAD/Sonst,\
        IGW/Soft,\
        IGW/DF,\
        IGW/SpecF,\
        IGW/Sonst,\
        AGW/Soft,\
        AGW/DF,\
        AGW/SpecF,\
        AGW/Sonst,\
        NR"', allow_blank=True)
    ticket_wos_validation.error = 'Your entry is not in the list'
    ticket_wos_validation.errorTitle = 'Invalid Entry'
    ticket_wos_validation.prompt = 'Please select from the list'
    ticket_wos_validation.promptTitle = 'List Selection'
    read_from_sheet.add_data_validation(ticket_wos_validation)
    c3 = read_from_sheet["G2"]
    c3.value = "Select"
    ticket_wos_validation.add(c3)

    #DAY

    ##DATA VALIDATION TICKET TYPE
    ticket_type_validation = DataValidation(type="list", formula1='"Incident, Problem, Request, Change, None"',
                                            allow_blank=True)
    ticket_type_validation.error = 'Your entry is not in the list'
    ticket_type_validation.errorTitle = 'Invalid Entry'
    ticket_type_validation.prompt = 'Please select from the list'
    ticket_type_validation.promptTitle = 'List Selection'
    read_from_sheet.add_data_validation(ticket_type_validation)
    c2 = read_from_sheet["M2"]
    c2.value = "Select"
    ticket_type_validation.add(c2)



    #PREPARING WRITE_TO_SHEET EXCEL
    #test_parser = read_from_sheet["A1"].value  # test
    #write_to_sheet["A1"] = test_parser  # test
    # log.info("write to worksheet")
    # write_to_sheet.cell(row=1, column=1).value = "DATE"
    # write_to_sheet["A2"] = "=Read_from!B2"
    # write_to_sheet.cell(row=1, column=2).value = "WEEK"  # test
    # write_to_sheet.cell(row=1, column=2).font = Font(bold=True)
    # write_to_sheet.cell(row=2, column=2).value="=WEEKNUM(A2)"


    log.info("Save workbook")
    workbook.save(filename="activity_to_clarity.xlsx")
    log.info(workbook.sheetnames)

    return workbook


def create_excel_if_not_exist():
    logger_setup()
    log = logging.getLogger("create_excel_if_not_exist()")
    log.info("start")
    try:
        workbook = load_workbook(filename="activity_to_clarity.xlsx")
        log.info("workbook loaded")
    except FileNotFoundError:
        log.info("call create_excel()")
        workbook = create_excel()
        log.info("workbook created")
        log = logging.getLogger("create_excel()")
        logger_setup()
        from GUI.gui_new import gui_new
        gui_new()
