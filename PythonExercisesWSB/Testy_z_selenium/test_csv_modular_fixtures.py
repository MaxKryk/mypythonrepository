import pytest


@pytest.fixture()
def csv_data():
    with open('data.csv') as f:
        lines = f.readlines()
    data = [line.strip() for line in lines]
    return data


@pytest.fixture()
def csv_header(csv_data):
    return csv_data[0]


@pytest.fixture()
def csv_records(csv_data):
    return csv_data[1:]


@pytest.fixture()
def column_names(csv_header):
    return csv_header.split(',')


def test_header_is_lowercase(csv_header):
    """Check if column names in header are lowercase"""
    assert csv_header == csv_header.lower()


def test_header_starts_with_id(column_names):
    """Check if the first column in header is id"""
    first_column_name = column_names[0]
    assert first_column_name == 'id'


def test_header_has_column_street(column_names):
    """Check if header has column street"""
    assert 'street' in column_names


def test_header_has_column_date(column_names):
    """Check if header has column date"""
    assert 'date' in column_names


def test_record_matches_header(column_names, csv_records):
    """Check if number of columns in each record matches header"""
    header_columns_count = len(column_names)
    errors = []
    for record in csv_records:
        record_values = record.split(',')
        record_values_count = len(record_values)
        if record_values_count != header_columns_count:
            errors.append(record)
    assert not errors


def test_record_first_field_is_number(csv_records):
    """Check if the first value in each record is a number"""
    errors = []
    for record in csv_records:
        record_values = record.split(',')
        if not record_values[0].isdigit():
            errors.append(record)
    assert not errors