import requests
import json
from time import time
from mock import Mock
from rates_downloader.nbp_rates_downloader import NBPRatesDownloader

############################################ Test 1 ################################################

# Tworzymy obiekt klasy NBPRatesDownloader
sciagaczka = NBPRatesDownloader()

# Testujemy co zwroci metoda _get_rate_api_url
api_url = sciagaczka._get_rate_api_url("USD")

# Jesli zmienna api_url nie zaczyna sie od 'http' to znaczy ze cos poszlo nie tak i chcemy
# wyrzucic blad (AssertionError)
assert api_url.startswith("http"), "Funkcja nie zwraca adresu HTTP!!"

# Dłuższa wersja, robi to samo:
# if not api_url.startswith("http:"):
#     raise AssertionError("Funkcja nie zwraca adresu HTTP!!")


############################################ Test 2 ################################################

# Chcemy sprawdzić, czy obsługiwany jest błąd przy podaniu nieprawidłowej waluty,
# podajemy więc nieistniejącą walutę "WSB" i 'łapiemy' błąd, który zwróci skrypt programisty.
try:
    sciagaczka.get_rate("WSB")
# Wg programisty, podanie nieprawidłowej waluty powinno przerwać działanie programu wyjątkiem
# ValueError z wiadomością jak poniżej.
except ValueError as err:
    if "Waluta prawdopodobnie nie istnieje na liscie NBP." in str(err):
        print("Nie ma takiej waluty, wiec test git")
    # Jeżeli wiadomość ValueErrora się nie zgadza, wyrzucamy wyjątek, żeby pokazać że test
    # nie powiódł się.
    else:
        raise ValueError("Wyskoczył ValueError z nieoczekiwanym komunikatem!")
# Jeżeli przy podaniu złej waluty nie wyskoczył żaden Error, my go podnosimy, ponieważ
# oczekiwaliśmy przecież, że się pojawi!
else:
    raise ValueError("Nie ma takiej waluty mordo a Ty mowisz, ze jest git wiec cos jest nie tak")


############################################ Test 3 ################################################


# Tutaj, chcemy zmierzyć czas wykonywania skryptu, pomijając czas zapytania na serwer NBP
# Wg specyfikacji, czas ten nie może przekroczyć 0.5s.

# Tworzymy klasę, która będzie 'udawała', że jest obiektem klasy Request, czyli będzie
# 'udawała, że jest internetem' ;)
class RequestsMock:

    def __init__(self, content):
        self.content = content
        self.text = content

    def json(self):
        return json.loads(self.content)


# Odpalamy skrypt raz i sprawdzamy co dokładnie zwraca API NBP.
# Ustawiamy sobie to jako dummy response serwera:
odp = '{"rates": [{"no": "042/A/NBP/2020", "effectiveDate": "2020-03-02", "mid": 4.1111}]}'
# Metoda get to jedyna metoda używana w projekcie. Chcemy ją więc zastąpić naszym udawanym obiektem:
requests.get = Mock()
# Ustawiamy, że gdy wywołamy metodę get - requests.get(cokolwiek), jako zwrotkę otrzymamy obiekt
# klasy RequestsMock z parametrami self.content i self.text równe zmiennej odp.
requests.get.return_value = RequestsMock(odp)

start_timer = time()
rate = sciagaczka.get_rate("EUR")
stop_timer = time()
assert stop_timer - start_timer < 0.5, "Skrypt zajmuje za dużo czasu!!!"
