########FOUND BY TAG
# #po podaniu kryterium zwraca liste utwory o danym tagu (jednym!)
#TODO odtwarzanie znalezionych utworow
from .main_menu import main_menu

def moreTracksFoundByTagDecision(self, elementToPlay, userInput):
    operationInput=input("What do you want to do with this track?\nplay or p to play, info or i to show info, main for MAIN")
    if operationInput.lower()=="play" or operationInput.lower()=="p":
        elementToPlay.play()
        moreTracksFoundByTagDecision(self, elementToPlay, userInput)
    elif operationInput.lower()=="info" or operationInput.lower()=="i":
        elementToPlay.showInfo()
        moreTracksFoundByTagDecision(self, elementToPlay, userInput)
    elif operationInput.lower()=="main" or operationInput.lower()=="m":
        main_menu(self)
    else:
        print("Unknown command, try again")
        moreTracksFoundByTagDecision(self, elementToPlay, userInput)
def oneTrackFoundByTag(Menu, chosenTag):


    for Track in Menu.sortedByTag:
        elementToPlay=Track()
        for Track in Menu.tracks:
            for tag in Track.tags:
                if tag==chosenTag:
                    elementToPlay=Track
                    print("One track found: ", elementToPlay.title)
            operationInput=input("What do you want to do? type play to play\ninfo to show info\nmain to return to main menu")
            if operationInput.lower()=="play" or operationInput.lower()=="p":
                elementToPlay.play()
                oneTrackFoundByTag(Menu)
            elif operationInput.lower()=="info" or operationInput.lower()=="i":
                elementToPlay.showInfo()
                oneTrackFoundByTag(Menu)
            elif operationInput.lower()=="main" or operationInput.lower()=="m":
                Menu.sortedByTag.clear()
                main_menu(Menu)
            else:
                print()
                print("Unknown command, try again")#red
                oneTrackFoundByTag(Menu)
def findByTagsInList(tracks, tagInput, foundByTags):
    foundByTags.clear()
    for Track in tracks:
        for tag in Track.tags:
            if tag==tagInput:
                foundByTags.append(Track)
    return foundByTags

