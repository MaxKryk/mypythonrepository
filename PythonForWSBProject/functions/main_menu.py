import os
import webbrowser


def main_menu(self):

    print("Hello, which operation do you want to execute? Type and press RETURN",
          #TODO

          "\n1 for MAIN MENU",
          "\n5a for search criterion track by author",
          "\n3 for search track by tags",
          "\n5t for search criterion track by title")
    user_choice_input=input()
    from PythonForWSBProject.functions.print_choice import print_choice
    if user_choice_input=="1":
        self.userChoice="1"
        print_choice(self)
        main_menu(self)



    elif user_choice_input=="3":
        self.userChoice="3"
        userInput = input("Type anything to start finding by tags or type MAIN to go to MAIN MENU")
        #self.tagsToFind.clear()
        from PythonForWSBProject.functions.type_tag_or_find_or_main import type_tag_or_find_or_main
        type_tag_or_find_or_main(self, userInput)


    elif user_choice_input=="5a" or user_choice_input=="5t":
        if(user_choice_input=="5a"):
            self.userChoice="a"
        elif(user_choice_input=="5t"):
            self.userChoice="b"
        criterion=''

#TODO
        print_choice(self)
        if (self.userChoice=="a"):
            userInput = input("Name author or type MAIN to go to MAIN MENU")
        elif (self.userChoice=="b"):
            userInput = input("Name title or type MAIN to go to MAIN MENU")
        if userInput.lower()=="MAIN".lower():
            main_menu(self)
        else:
            for track in self.tracks:
                if (self.userChoice=="a"):
                    if track.author.lower()==userInput.lower():
                        self.sorted_by_criterion.append(track)
                elif (self.userChoice=="b"):
                    if track.title.lower()==userInput.lower():
                        self.sorted_by_criterion.append(track)

            #NO TRACKS FOUND================================================+==========================================
            if (len(self.sorted_by_criterion)==0):
                print("No tracks from the given author")
                main_menu(self)

            #ONE TRACK FOUND=============================================================================================
            elif (len(self.sorted_by_criterion)==1):
                from PythonForWSBProject.functions.one_track_found_by_criterion import one_track_found_by_criterion
                one_track_found_by_criterion(self)

            #MORE TRACKS FOUND==========================================================================================
            elif (len(self.sorted_by_criterion)>1):
                from PythonForWSBProject.functions.more_tracks_found_by_criterion import more_tracks_found_by_criterion
                more_tracks_found_by_criterion(self, userInput)

            else:
                print("Unknown command, try again")
                main_menu(self)