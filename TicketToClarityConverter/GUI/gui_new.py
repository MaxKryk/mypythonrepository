
from tkinter import *
from tkinter import messagebox

from GUI.cw_logic import fill_cw_options
from GUI.file_operations import erase_file
from GUI.main_logic import main_logic
from GUI.create_excel import create_excel_if_not_exist
def gui_new():
    window = Tk()

    window.title("Activity to clarity converter")
    window.geometry('250x350')
    #DECLARATIONS
    lbl_title = Label(window, text="Activity to clarity converter")
    lbl_info = Label(window, text="Tell me what to do!")



    lbl_info_sel_weeknum = Label(window, text="SELECT WEEKNUMBER")
    #extract week numbers
    #tk_var=StringVar(window)
    chosen_cw = StringVar(window)
    cw_options=[]
    filled_cw_options=fill_cw_options(cw_options)
    print("filled cw")

    print(filled_cw_options)

    chosen_cw.set(filled_cw_options[0])

    dropdown_cw = OptionMenu(window, chosen_cw, *filled_cw_options)

    def clicked_convert():
        current_cw=int(chosen_cw.get())
        print("current cw ", current_cw)
        print("type of current cw ", type(current_cw))
        main_logic(current_cw)
        lbl_info.configure(text="TICKETS WERE CONVERTED!")
       # messagebox.showinfo('TICKETS WERE CONVERTED', 'Please check the outputfile in the directory with the program')



    btn_convert=Button(window, text="Convert!", bg="orange", fg="white", command=clicked_convert)

    btn_create_excel=Button(window, text="CREATE XLSX IF DOES NOT EXIST", bg="green", fg="white", command=create_excel_if_not_exist)
    #GRID
    lbl_title.grid(column=0, row=0)
    btn_create_excel.grid(column=0, row=1)






    #opt.pack()


    lbl_info_sel_weeknum.grid(column=0, row=3)
    dropdown_cw.grid(column=0, row=4)

    btn_convert.grid(column=0, row=6)
    lbl_info.grid(column=0, row=7)

    window.mainloop()

gui_new()