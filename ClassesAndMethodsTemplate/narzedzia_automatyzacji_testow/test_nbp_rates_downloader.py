import pytest
import json
from time import time

from .nbp_rates_downloader import NBPRatesDownloader

def test__get_available_currencies():
    NBPRatesDownloader().get_available_currencies()

def test__get_rate_api_url2():
    NBPRatesDownloader()._get_rate_api_url("EUR","01/01/2000", "01/01/2001")

def test__convert_date_to_api_format():
    NBPRatesDownloader()._convert_date_to_api_format("01/01/2000")

def test__convert_date_to_api_format2():
    with pytest.raises(ValueError) as err:
        NBPRatesDownloader()._convert_date_to_api_format("2000/01/01")

def test__convert_date_to_api_format3():
    NBPRatesDownloader()._convert_date_to_api_format("")

def test__get_rate_api_url(nbp_object_mocked):
    api_url = nbp_object_mocked._get_rate_api_url("USD")
    assert api_url.startswith("http"), "Funkcja nie zwraca adresu HTTP!!"


def test_negatywny(nbp_object_mocked_fail):
    with pytest.raises(ValueError) as err:
        NBPRatesDownloader().get_rate("WSB")
    if "Waluta prawdopodobnie nie istnieje na liscie NBP." not in str(err.value):
        raise ValueError("WyskoczyĹ‚ ValueError z nieoczekiwanym komunikatem!")


def test_performance(nbp_object_mocked):
    start_timer = time()
    rate = nbp_object_mocked.get_rate("EUR")
    stop_timer = time()
    assert stop_timer - start_timer < 0.5, "Skrypt zajmuje za duĹĽo czasu!!!"


def mock_request_get_with_response(response):
    from mock import Mock
    import requests

    class RequestsMock:

        def __init__(self, content):
            self.content = content
            self.text = content

        def json(self):
            return json.loads(self.content)

    requests.get = Mock()
    requests.get.return_value = RequestsMock(response)


@pytest.fixture()
def nbp_object_mocked_fail():
    mock_request_get_with_response('Error 404')
    return NBPRatesDownloader()


@pytest.fixture()
def nbp_object_mocked():
    mock_request_get_with_response(
        '{"rates": [{"no": "042/A/NBP/2020", "effectiveDate": "2020-03-02", "mid": 4.1111}]}')
    return NBPRatesDownloader()