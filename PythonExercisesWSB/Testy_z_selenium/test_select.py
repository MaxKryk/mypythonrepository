import pytest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.select import Select


@pytest.fixture(scope="session")
def chromedriver(request):
    driver = webdriver.Chrome(executable_path="C:\\Users\\makryk\\IdeaProjects\\mypythonrepository\\PythonExercisesWSB\\Testy_z_selenium\\chromedriver\\chromedriver.exe")

    def chromedriver_teardown():
        driver.close()

    request.addfinalizer(chromedriver_teardown)
    return driver


@pytest.fixture(scope="session")
def prestashop_driver(chromedriver):
    """
    :type chromedriver: WebDriver
    """
    chromedriver.get("http://demo.prestashop.com/")
    chromedriver.switch_to.frame(0)
    return chromedriver


def test_select(prestashop_driver):
    driver = prestashop_driver
    driver.get("http://fo.demo.prestashop.com/en/men/1-1-hummingbird-printed-t-shirt.html#/size-s/color-white")
    select = Select(driver.find_element_by_id("group_1"))
    select.select_by_visible_text("M")

    import time
    time.sleep(10)