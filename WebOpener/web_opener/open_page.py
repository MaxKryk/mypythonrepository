from selenium import webdriver
from datetime import datetime
import time
import os

filepath=os.path.dirname(__file__)
print("przed zmiana" + filepath)
filepath=filepath.replace("/web_opener","")
print("po" + filepath)
filepath=filepath.replace("/", "\\")
filepath=os.path.join(filepath, "chromedriver.exe")
filepath=filepath.replace("\\", "\\\\")
print("relfpath:",  filepath)

#chromedriver="C:\\Users\\makryk\\IdeaProjects\\mypythonrepository\\WebOpener\\chromedriver.exe"
chromedriver=filepath


#chromedriver=r"%s"%filepath
driver=webdriver.Chrome(chromedriver)



url="http://www.google.com/"
url1="https://stackoverflow.com/questions/28431765/open-web-in-new-tab-selenium-python"
url2="https://yagisanatode.com/2018/02/26/how-to-display-and-entry-in-a-label-tkinter-python-3/"
urls=[url, url1, url2]

output_strings=[]

def open_and_close_cycle(url):
   browser= webdriver.Chrome(chromedriver)
   browser.get(url)
   str=""
   str=str+("%s: " %browser.title)
   now=datetime.now()
   current_time=now.strftime("%H:%M:%S")
   str=str+("opened and closed on %s: " %current_time)
   browser.close()
   output_strings.append(str)

def perform_iteration(urls):
    for url in urls:
        open_and_close_cycle(url)
    for str in output_strings:
        print(str)
    output_strings.clear()

def start_logic(urls):
    for x in range(1, 10):
        print("\n===START ITERATION %i===\n" %x)
        perform_iteration(urls)
        print("\n===END ITERATION===\n")
        print("Wait 30 minutes...")
        time.sleep(1800)
#start_logic(urls)
