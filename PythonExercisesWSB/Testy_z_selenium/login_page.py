from selenium import webdriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from pages.page_base import PageBase


class LoginPage(PageBase):
    def __init__(self, driver):
        super().__init__(driver)

    @property
    def header_logo(self):
        """
        :rtype: WebElement
        """
        return self.driver.find_element_by_css_selector('.logo')

    @property
    def label_subpage_title(self):
        return self.driver.find_element_by_css_selector('.page-header > h1:nth-child(1)')

    @property
    def input_email(self):
        return self.driver.find_element_by_xpath("//input[@name='email']")

    def is_open(self):
        try:
            element = WebDriverWait(self.driver, 10).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, '.page-header > h1:nth-child()')))
            return True
        except Exception as e:
            print(e)
            return False