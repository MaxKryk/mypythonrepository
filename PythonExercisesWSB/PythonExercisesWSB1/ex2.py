#wyprowadza liczby pierwsze od 0 do zadanej przez użytkownika

#dla danej liczby n należy sprawdzić, czy dzieli się ona kolejno przez 2, 3, aż do n−1.
#Jeśli przez żadną z nich się nie dzieli, oznacza to, że jest pierwsza

def jesliPierwszaWyswietl(doSprawdzenia):
    n=doSprawdzenia
    licznik = 0
    for x in range (2, n-1):
        if (n%x!=0):
            licznik=licznik+1
            continue
    if licznik==((n-1)-2):
        print(n)
#start programu
print("Podaj liczbę")
user_input=int(input())
print("Liczby pierwsze:")
for x in range (0, user_input+1):
    jesliPierwszaWyswietl(x)
