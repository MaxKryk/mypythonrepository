import webbrowser
import os



class Track():

    def _init_(self, filename, title, author, tags, describtion):
        self.filename=filename
        self.filepath=os.path.join(os.path.join(os.path.expanduser("~"), "musicDirectory", self.filename))
        self.title=title
        self.author=author
        self.tags=tags
        self.describtion=describtion
    def Function(self):
        print ("This is track's function")
    def play(self):
        print(self.title, " is playing...")
        webbrowser.open(self.filepath)
    def showInfo(self):
        print("\nABOUT TRACK\ntitle ", self.title, "\nauthor: ", self.author, "\nfilename: ", \
              self.filename.upper(),
              "\ntags ", self.tags, "\ndescribtion: ", self.describtion, "\n=====")

