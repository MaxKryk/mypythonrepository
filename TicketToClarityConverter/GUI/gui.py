from tkinter import *

from GUI.create_excel import create_excel_if_not_exist
def gui():
    window = Tk()

    window.title("Activity to clarity converter")
    window.geometry('250x150')
    #DECLARATIONS
    lbl_title = Label(window, text="Activity to clarity converter")
    lbl_info = Label(window, text="Tell me what to do!")

    #btn_convert=Button(window, text="Convert!", bg="orange", fg="white", command=main_logic)
    def execute_close_old_gui_and_destroy():
        window.destroy()
        create_excel_if_not_exist()


    btn_create_excel=Button(window, text="CREATE XLSX IF DOES NOT EXIST", bg="green", fg="white", command=execute_close_old_gui_and_destroy)
    #GRID
    lbl_title.grid(column=0, row=0)
    btn_create_excel.grid(column=0, row=1)
    #btn_convert.grid(column=0, row=3)
    lbl_info.grid(column=0, row=4)
    window.mainloop()


