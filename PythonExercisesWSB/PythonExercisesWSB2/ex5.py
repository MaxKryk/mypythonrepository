#3- stwprzyc funkcje z nieograniczona liczba argumentow - lancuchow tekstowych i jeden parametr domyslny: glue, ktory
#jest = ':'
#polaczyc wszystkie lancuchy, ktore maja wiecej niz 3 symbole, dla polaczenia wykorzystac operator glue.

def functionReturnStrings(self, *args):
    glue="="
    listOfStrings=list(args)
    listMoreThen3=[]
    for element in listOfStrings:
        if (len(element)>=3):
            listMoreThen3.append(element)
    return(listMoreThen3)

print(functionReturnStrings("hello", "hello1", "world", "python", "a", "b", "hw", "jk", "mk"))