from PythonForWSBProject.functions.main_menu import main_menu

def more_tracks_found_by_tag_decision(self, elementToPlay, userInput):
    operationInput=input("What do you want to do with this track?\nplay or p to play, info or i to show info, main for MAIN")
    if operationInput.lower()=="play" or operationInput.lower()=="p":
        elementToPlay.play()
        more_tracks_found_by_tag_decision(self, elementToPlay, userInput)
    elif operationInput.lower()=="info" or operationInput.lower()=="i":
        elementToPlay.showInfo()
        more_tracks_found_by_tag_decision(self, elementToPlay, userInput)
    elif operationInput.lower()=="main" or operationInput.lower()=="m":
        main_menu(self)
    else:
        print("Unknown command, try again")
        more_tracks_found_by_tag_decision(self, elementToPlay, userInput)