from GUI.check_wos import check_wos

from GUI.logging import logger_setup
import logging

def append_to_lists(all_entries_arr, ea_arr, activity_arr, solved_to_process_arr, problem_arr, change_arr, request_arr):


    #IN: all_entries_array
    #EMPTY LISTS:
    #GROUP 1
    # ea=[]
    # #GROUP 2
    # activity=[]
    # #GROUP 3 inc_ana_wos_env []{51293257; 1}
    # solved_to_process=[]

    # #TODO igw
    # #GROUP 4
    # change=[]
    # request=[]
    # problem=[]
    #OUT:
    #  filled lists:
    #GROUP 1
    # ea=[]
    # #GROUP 2
    # activity=[]
    # #GROUP 3 inc_ana_wos_env []{51293257; 1}
    # solved_to_process=[]

    # #TODO igw
    # #GROUP 4
    # change=[]
    # request=[]
    # problem=[]

    logger_setup()
    log = logging.getLogger("append_to_lists")
    log.info("start")

    #todo change "[Mi] C#49727260 - Versicherungspakete (4h)"
    log.info("all elem array length")
    log.info(len(all_entries_arr))
    #dla kazdej tablicy z tablicy tablic\
    log.info("len arr-1")
    log.info(len(all_entries_arr) - 1)
    for x in range (0, len(all_entries_arr)):
        #GROUP 1 ea
        if all_entries_arr[x][0]== "":
            log.info("empty cell")
            pass
        elif all_entries_arr[x][0]== 'EA':
            log.info("activity is ea")
            ea_arr.append(all_entries_arr[x])
            log.info("added to ea: ", all_entries_arr[x][0], " ", all_entries_arr[x][1], all_entries_arr[x][2])
        #GROUP 2 mim dispatch kt
        elif all_entries_arr[x][0]== 'MIM':
            log.info("activity is MIM")
            activity_arr.append(all_entries_arr[x])
        elif all_entries_arr[x][0]== 'DISPATCH':
            log.info("activity is DISPATCH")
            activity_arr.append(all_entries_arr[x])
        elif all_entries_arr[x][0]== 'KT':
            log.info("activity is KT")
            activity_arr.append(all_entries_arr[x])
            #write array content to file
            log.info("activity filled")
            log.info(activity_arr)
        #GROUP 3 incidents
        elif all_entries_arr[x][0]== 'Solved':#TODO

            print("activity is solved: ", all_entries_arr[x])
            solved_to_process_arr.append(all_entries_arr[x])
            print(all_entries_arr[x], ' has been appended to the array solved_to_process')


        #GROUP 4
        #reading_excel append_to_lists()

        elif all_entries_arr[x][0]== 'Problem':
            log.info("activity is problem")
            problem_arr.append(all_entries_arr[x])
            #TEST
        elif all_entries_arr[x][0]== 'Change':
            log.info("activity is change")
            change_arr.append(all_entries_arr[x])
            log.info("change appended")
        elif all_entries_arr[x][0]== 'Request':
            log.info("activity is request")
            request_arr.append(all_entries_arr[x])
        else:
            pass


    log.info('APPENDING TO LIST FINISHED')
    filehandle =open("entries_output.txt", "a")
    #WRITE TO FILE

    #grupa 1 ea
    ##create_ea(ea)
    #grupa 2 activity
    ##create_activity(activity)
    #grupa 3 incidents

    #grupa 4 change request problem
    ##log.info("change")
    #create_change_request_problem(change, "C#")
    #request
    #log.info("request")
##create_change_request_problem(request, "R#")
#problem
#log.info("problem")
#create_change_request_problem(problem, "P#")
