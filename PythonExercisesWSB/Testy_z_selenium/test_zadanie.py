import time
import json
import pytest
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.remote.webdriver import WebDriver

#Maximilian Kryk - testy w selenium
EMAIL = "tester@wp.pl"
PASSWORD = "tester@wp.pl"
#https://www.kainos.pl/blog/zaawansowane-interakcje-w-selenium-webdriver/
#https://testelka.pl/selektory-w-selenium-najprostsze-metody/

@pytest.fixture(scope="session")
def chromedriver(request):
    driver = webdriver.Chrome(executable_path="C:\\Users\\makryk\\IdeaProjects\\mypythonrepository\\PythonExercisesWSB\\Testy_z_selenium\\chromedriver\\chromedriver.exe")

    def chromedriver_teardown():
        driver.close()

    request.addfinalizer(chromedriver_teardown)
    return driver


@pytest.fixture(scope="session")
def prestashop_driver(chromedriver):
    """
    :type chromedriver: WebDriver
    """
    chromedriver.get("http://demo.prestashop.com/")
    chromedriver.switch_to.frame(0)
    return chromedriver


def test_main_page(prestashop_driver):
    assert "PrestaShop Demo" in prestashop_driver.title

#Zalogować się prawidłowymi danymi użytkownika (konto powinno być stworzone przed przystąpieniem do tworzenia testu)
def test_user_login(prestashop_driver):
    """
    :param prestashop_driver:
    :type prestashop_driver: WebDriver
    """
    driver = prestashop_driver
    sign_in_button=driver.find_element(By.XPATH, "//span[contains(.,'Sign in')]")
    sign_in_button.click()

    mail=driver.find_element_by_name("email")
    mail.send_keys(EMAIL)
    password=driver.find_element_by_name("password")
    password.send_keys(PASSWORD)
    submit_login=driver.find_element(By.XPATH, "//button[@id='submit-login']")
    submit_login.click()
    time.sleep(1)
    username_label = driver.find_element_by_css_selector("#_desktop_user_info > div > a.account > span")
    assert username_label.text == "Tester Testerski"
    # Tutaj wprowadz swoj kod
    #assert False

#Przejść do kategorii (podstrony) np. "Home / Art"
def test_subpage_navigate_to_art_category(prestashop_driver):
    """
    :param prestashop_driver:
    :type prestashop_driver: WebDriver
    """
    driver = prestashop_driver
    art_button=driver.find_element(By.XPATH, "//a[contains(text(),'Art')]")
    art_button.click()
    time.sleep(3)

    category_label = driver.find_element_by_css_selector("#main > div.block-category.card.card-block.hidden-sm-down > h1")
    assert category_label.text == "ART"

#Wybrać dowolny (dostępny) produkt np. "TODAY IS A GOOD DAY FRAMED POSTER"
#Testy sie wysypuja - brak wymaganej liczby produktow
def test_select_product_from_produts_list(prestashop_driver):
    """
    :param prestashop_driver:
    :type prestashop_driver: WebDriver
    """
    driver = prestashop_driver

    select_product = driver.find_element_by_css_selector("#js-product-list > div.products.row > article:nth-child(5) > div > a > img")
    select_product.click()

    time.sleep(3)

    product_label = driver.find_element_by(By.CSS_SELECTOR, "css=.product-miniature:nth-child(3) .product-description a")
    assert product_label.text == "TODAY IS A GOOD DAY FRAMED POSTER"

#Przejść do strony z koszykiem
def test_product_order(prestashop_driver):
    """
    :param prestashop_driver:
    :type prestashop_driver: WebDriver
    """
    driver = prestashop_driver

    select_size = driver.find_element_by_css_selector("#group_3")
    select_size.select_by_visible_text("80x120cm")

    time.sleep(1)

    add_to_cart = driver.find_element_by_css_selector("#add-to-cart-or-refresh > div.product-add-to-cart > div > div.add > button")
    add_to_cart.click()

    time.sleep(1)

    add_label = driver.find_element_by_css_selector("#blockcart-modal > div > div > div.modal-body > div > div.col-md-5.divide-right > div > div:nth-child(2) > span")
    assert add_label.text == "Dimension: 80x120cm"

    proceed_to_checkout = driver.find_element_by_css_selector("#blockcart-modal > div > div > div.modal-body > div > div.col-md-7 > div > div > a")
    proceed_to_checkout.click()

    time.sleep(3)

    proceed_label = driver.find_element_by_css_selector("#main > div > div.cart-grid-body.col-xs-12.col-lg-8 > div > div.card-block > h1")
    assert proceed_label.text == "SHOPPING CART"
#UWAGA!
#tutaj testy sie wysypuja, co jest spowodowane brakiem towaru w wymaganej ilosci
#Z poziomu koszyka zmienić ilość na 2 szt.
def test_cart_edit_quantity(prestashop_driver):
    """
    :param prestashop_driver:
    :type prestashop_driver: WebDriver
    """
    driver = prestashop_driver
    up_quantity=driver.find_element(By.XPATH, "//section[@id='main']/div/div/div/div[2]/ul/li/div/div[3]/div/div[2]/div/div/div/span[3]/button/i")
    up_quantity.click()
    # Tutaj wprowadz swoj kod
    #assert False
    quantity_label = driver.find_element_by_css_selector("#cart-subtotal-products > span.label.js-subtotal")
    assert quantity_label.text == "2 items"

#Sfinalizować zakup (wprowadzić dane adresowe i przejść przez kolejne kroki z odpowiednią weryfikacją zawartości)
def test_cart_finalize_order(prestashop_driver):
    """
    :param prestashop_driver:
    :type prestashop_driver: WebDriver
    """
    driver = prestashop_driver

    driver = prestashop_driver

    proceed_button = driver.find_element_by_css_selector("#main > div > div.cart-grid-right.col-xs-12.col-lg-4 > div.card.cart-summary > div.checkout.cart-detailed-actions.card-block > div > a")
    proceed_button.click()

    time.sleep(1)

    address_label = driver.find_element_by_css_selector("#id-address-delivery-address-7383 > header > label > span.address-alias.h4")
    assert address_label.text == "My Address"

    continue_button = driver.find_element_by_css_selector("#checkout-addresses-step > div > div > form > div.clearfix > button")
    continue_button.click()

    time.sleep(1)

    shipping_label = driver.find_element_by_css_selector("#js-delivery > div > div.delivery-options > div.row.delivery-option > label > div > div.col-sm-4.col-xs-12 > span")
    assert shipping_label.text == "Delivery next day!"

    continue_button = driver.find_element_by_css_selector("#js-delivery > button")
    continue_button.click()

    time.sleep(1)

    payment_label = driver.find_element_by_css_selector("#payment-option-2-container > label > span")
    assert payment_label.text == "Pay by bank wire"

    choice_button = driver.find_element_by_css_selector("#payment-option-2")
    choice_button.click()

    time.sleep(3)

    agree_button = driver.find_element_by_css_selector("#conditions_to_approve\[terms-and-conditions\]")
    agree_button.click()

    time.sleep(3)

    agree_label = driver.find_element_by_css_selector("#conditions-to-approve > ul > li > div.condition-label > label")
    assert agree_label.text == "I agree to the terms of service and will adhere to them unconditionally."

    order_button = driver.find_element_by_css_selector("#payment-confirmation > div.ps-shown-by-js > button")
    order_button.click()

    time.sleep(2)

    order_label = driver.find_element_by_css_selector("#content-hook_order_confirmation > div > div > div > p")
    assert order_label.text == "An email has been sent to your mail address tester@wp.pl."

