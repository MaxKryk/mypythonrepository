from GUI.append_to_lists import append_to_lists
from GUI.file_operations import erase_file
import sys
import logging
from openpyxl import load_workbook

from GUI.logging import logger_setup


def count_entries(wb):#TODO is this function really needed?
    logger_setup()
    log = logging.getLogger("count entries()")
    workbook = load_workbook(filename=wb)
    log.info("wb loaded")
    counter=0
    read_from_sheet = workbook.get_sheet_by_name("Read_from")
    log.info("sheet marked: " + read_from_sheet.title)
    #take first row
    for element in read_from_sheet.iter_rows(min_row=1,
                                             min_col=1,
                                             max_col=1,

                                             values_only=True):
        #log.info(element)
        counter=counter+1
    log.info("Counter value: ")
    log.info(counter)

    pass

#converts excel to all_entries_array
#in: empty all_entries_array to fill
#out: filled all_entries_array

def convert_excel_to_all_entries_array(arr, kw_num):
    logger_setup()
    log = logging.getLogger("convert_excel_to_all_entries_array")

    erase_file()
    sys.path.append(('.'))
    log.info("load workbook...")
    workbook = load_workbook(filename="activity_to_clarity.xlsx", data_only=True)
    log.info("get sheet...")
    read_from_sheet = workbook.get_sheet_by_name("Read_from")
    log.info("count entries...")
    count_entries("activity_to_clarity.xlsx")
    log.info("Create all_entries_array...")

    for element in read_from_sheet.iter_rows(min_row=2,
                                             min_col=1,
                                             values_only=True):
        log.info("required kw: ", str(kw_num), "KW: ", element[14])
        log.info("Kw_num type: ", type(kw_num), " element[14] type: ", type(element[14]))
        if element[14]==None:
            log.info("empty cw cell, element has not been appended")
            pass
        elif element[14]==kw_num:
            log.info("match, element has been appended")
            arr.append(element)
        else:
            log.info("no match, element has not been appended")




    #append_to_lists(all_entries_array)
    pass

arr=[]
convert_excel_to_all_entries_array(arr, 52)
print("output from kw 52")
for element in arr:
    if element==None:
        pass
    else:
        print(element[14])








