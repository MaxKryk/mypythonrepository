from PythonForWSBProject.functions.main_menu import main_menu
from PythonForWSBProject.functions.one_track_found_by_tag import one_track_found_by_tag
def type_tag_or_find_or_main(self, userInput):

    if userInput.lower()=="MAIN".lower():
        main_menu(self)
    else:
        print("Type the next tag or type F or FIND to find or M or MAIN to go to MAIN MENU")
        userDecision=input()
        if(userDecision.lower()=="f" or userDecision.lower()=="find"):
            print("Tags to find:")
            if len(self.tagsToFind)==1:
                print(self.tagsToFind)
                chosenTag=self.tagsToFind[0]
                print("FINDING TRACKS WITH TAG: ", chosenTag)
                one_track_found_by_tag(self, chosenTag)
            elif (len(self.tagsToFind)>1):
                print(self.tagsToFind)
                #TODO
                print("This functionality will be added in the future in the next update, thanks for supporting us")
                #podaje liste tagow
                #sprawdza czy wszystkie te tagi sa w w liscie tagow kazdego utworu po kolei
                #jesli tak to wrzuca je do listy z wynikami
            else:
                print("No tags to find, type them again")
                type_tag_or_find_or_main(self, userInput)
            print("FINDING")
        elif(userDecision.lower()=="m" or userDecision.lower()=="main"):
            main_menu(self)
        else:
            self.tagsToFind.append(userDecision)
            type_tag_or_find_or_main(self, userInput)