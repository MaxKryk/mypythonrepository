import datetime
from GUI.logging import logger_setup
from GUI.transform_to_clarity_convention import transform_to_clarity_convention_for_inc_ana_wad
import logging

#in: array with sorted entries (e.g. ea, inc_ana_wad, etc)
#out: write to file
#grupa 1 ea
def create_ea(arr):
    #TODO
    print("ea arr")
    print(arr)
    print("/ea arr")
    logger_setup()
    log = logging.getLogger("create_ea")

    log.info("ea checkpoint")
    ea_output=[]

    log.info(ea_output)
    log.info("checkpoint")

    row_length=(len(arr))
    log.info("row", row_length)
    col_length=(len(arr[0])-1)
    log.info("col", col_length)
    try:
        for row in range (0, row_length):
            for col in range (0, col_length):
                log.info("loop: row: ", row, "col ", col)
                tempList=[]
                tempList.append(arr[col][9])
                tempList.append(arr[col][2])
                tempList.append(arr[col][7])
                ea_output.append(tempList)
                log.info("tempList ", tempList)
                log.info("ea output", ea_output)
                log.info("col loop end")

    except IndexError as e:
        log.info("row loop end")
        log.info(e)
        pass

    filehandle =open("entries_output.txt", "a")
    filehandle.write("ERSTANA")
    filehandle.write("\n")
    #write to file
    for listitem in ea_output:
        filehandle.write('%s\n' % listitem)
    filehandle.close()

#grupa 2 activity #todo
def create_activity(arr):
    logger_setup()
    log = logging.getLogger("create_activity()")
    log.info(" activity checkpoint")
    activity_output=[]

    log.info(activity_output)
    log.info("checkpoint")

    row_length=(len(arr))-1
    log.info("row", row_length)
    col_length=(len(arr[0])-1)
    log.info("col", col_length)
    try:
        for row in range (0, row_length):
            for col in range (0, col_length):
                log.info("loop: row: ", row, "col ", col)
                tempList=[]
                tempList.append(arr[col][0]) #TODO TU KONTYNUOWAC
                tempList.append(arr[col][9])
                tempList.append(arr[col][7])
                if (arr[col][13] == None):
                    pass
                else:
                    tempList.append(arr[col][13])
                activity_output.append(tempList)
                log.info("tempList ", tempList)
                log.info("col loop end")
        log.info("activity output", activity_output)
    except IndexError as e:
        log.info("row loop end")
        log.info(e)
        pass

    filehandle =open("entries_output.txt", "a")
    filehandle.write("\nACTIVITY")
    filehandle.write("\n")
    #TODO dodaj sortowanie alfabetyczne listy
    #write to file
    for listitem in activity_output:
        filehandle.write('%s\n' % listitem)
    filehandle.close()

#grupa 3 incidents [day]{nr ticketa, time}
def create_inc_ana_wad_igw(arr, env, is_igw):#, env): #todo improvement - flexible enviroment
    logger_setup()
    log = logging.getLogger("create_inc_ana_wad()")
    log.info("\narr as parameter content")
    log.info(arr)
    log.info("env: ", env)
    log.info("create_inc_ana_wad checkpoint")
    account = ""
    if (is_igw==True):
        account= "DAI-GO-R61-GEW"
    else:
        if (env=="PRU"):
            account= "INC-ANA-WAD-PRU"
        elif (env=="WAU"):
            account= "INC-ANA-WAD-WAU"
        elif (env=="INU"):
            account= "INC-ANA-WAD-INU"
        elif (env=="RTU"):
            account= "INC-ANA-WAD-RTU"
        else:
            pass
    #depends on the enviroment given as parameter
    log.info("empty inc ana wad, here you the logic will append tickets")
    inc_ana_wad_igw=[] #todo sprawdzic czy tablica jest pusta, jesli jest pelna to wyczysc
    if len(inc_ana_wad_igw)!=0:
        log.warning("LIST INC ANA WAD SHOULD BE EMPTY!!!!")
    log.info(inc_ana_wad_igw)
    log.info("checkpoint")
    row_length=len(arr)
    log.info("rows: ", row_length)
    for row in range (0, row_length):
        #log.info("row index: ", row)
        tempList=[]
        #tempString =arr[col][9] #todo structure of the comment
        #tempList.append()
        #
        #               tempString =arr[col][9]
        log.info(arr[row][5])
        if (is_igw==True):
            tempList.append(arr[row][9])
            log.info('arr[col][9]: ', arr[row][9])
            tempList.append(arr[row][2])
            log.info('arr[col][2]: ', arr[row][2])
            tempList.append(arr[row][7])
            log.info('arr[col][7]: ', arr[row][7])
            inc_ana_wad_igw.append(tempList)
            log.info("tempList appended: ", tempList)
            log.info("iteration end")
        else:
            if arr[row][5]==env:
                tempList.append(arr[row][9])
                log.info('arr[col][9]: ', arr[row][9])
                tempList.append(arr[row][2])
                log.info('arr[col][2]: ', arr[row][2])
                tempList.append(arr[row][7])
                log.info('arr[col][7]: ', arr[row][7])
                inc_ana_wad_igw.append(tempList)
                log.info("tempList appended: ", tempList)
                log.info("iteration end")
            else:
                pass

    print("inc_ana_wad_igw elements123")
    for element in inc_ana_wad_igw:
        print(element)
    filehandle =open("entries_output.txt", "a")

    title=str("\n"+account+ "\n")


    filehandle.write(title)#todo5
    log.info("title written to file, title: ", account)


    for element in inc_ana_wad_igw:
         element=transform_to_clarity_convention_for_inc_ana_wad(element)
         log.info("written to file ", type(element), " : ", element)
         filehandle.write('%s' % element)

    filehandle.close()

#grupa 4 change, requests, problems [day]R/C/P#nr ticketa - time}

def create_change_request_problem(arr, type_letter):
    logger_setup()
    log = logging.getLogger("create_change_request_problem()")
    #example
    #[Mo] C#51707338 - Anlage neues TeDa Feld - T269 "Automobilmarke" (3h)


    log.info("change_request_problem checkpoint")
    #letter to ticket type fullname
    type_fullname=''
    if type_letter=='C#':
        type_fullname='CHANGE'
    elif type_letter=='R#':
        type_fullname='REQUEST'
    elif type_letter=='P#':
        type_fullname='PROBLEM'
    log.info("var typ_fullname set to ", type_fullname)
    log.info('#elements of the list need to be converted to output file:')
    log.info(arr)
    change_request_problem_output=[] #this list should be filled by logic
    log.info("checkpoint change_request_problem_output 2")

    row_length=(len(arr))
    log.info("row: ", row_length)
    col_length=(len(arr[0])-1)
    log.info("col: ", col_length)

    try:

        for row in range (0, row_length):
            for col in range (0, col_length):
                log.info("loop: row: ", row, "col ", col)
                print("heloha")
                tempList=[]
                tempList.append(arr[col][9])
                tempList.append(type_letter)
                tempList.append(arr[col][2])
                tempList.append(arr[col][4])
                tempList.append(arr[col][7])
                change_request_problem_output.append(tempList)
                log.info("change_request_problem_output tempList ", tempList)
                log.info("checkpoint change_request_problem_output: ", change_request_problem_output)
                log.info("col loop end")
    except IndexError as e:
        log.info("ERROR")
        log.info("row loop end")
        log.info(e)
        pass

    filehandle =open("entries_output.txt", "a")
    log.info("set header via type_fullname", type_fullname)
    filehandle.write("\n"+type_fullname)
    filehandle.write("\n")
    #write to file
    for listitem in change_request_problem_output:
        log.info(listitem)
        log.info("written to file")
        filehandle.write('%s\n' % listitem)
    filehandle.close()









