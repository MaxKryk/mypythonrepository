from openpyxl import load_workbook
import logging

from GUI.gui import gui

from GUI.logging import logger_setup


def main():
    logger_setup()
    log=logging.getLogger("solved_inc_to_inc_ana_wad")

    try:
        workbook = load_workbook(filename="activity_to_clarity.xlsx", data_only=True)
        log.info("file exists")
        from GUI.gui_new import gui_new
        gui_new()
    except:
        log.info("no file")
        gui()




