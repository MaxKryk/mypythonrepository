import pytest
from selenium import webdriver
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.common.keys import Keys


@pytest.fixture(scope="session")
def chromedriver(request):
    driver = webdriver.Chrome(executable_path="webdriver\\chromedriver.exe")

    def chromedriver_teardown():
        driver.close()

    request.addfinalizer(chromedriver_teardown)
    return driver


@pytest.fixture(scope="session")
def driver(chromedriver):
    """
    :type chromedriver: WebDriver
    """
    chromedriver.get("http://demo.prestashop.com/")
    chromedriver.switch_to.frame(0)
    return chromedriver


def test_main_page_screenshot(driver):
    if "PrestaShop Demo" in driver.title:
        driver.save_screenshot("prestashop_screenshot.jpg")
    assert "PrestaShop Demo" in driver.title