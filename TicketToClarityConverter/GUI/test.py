from tkinter import *

root = Tk()
root.title("Tk dropdown example")

mainframe = Frame(root)
mainframe.grid(column=0, row=0, sticky=(N,W,E,S))
mainframe.columnconfigure(0, weight=1)
mainframe.rowconfigure(0, weight=1)
mainframe.pack(pady=100, padx=100)

tkvar1 = StringVar(root)
#tkvar2 = StringVar(root)

choices_wl = [2, 3, 4]
#choices_rw = [5, 10, 15, 20]

popupMenu = OptionMenu(mainframe, tkvar1, *choices_wl)
Label(mainframe, text="Select a number").grid(row=1, column=1)
popupMenu.grid(row=2, column=1)

#popupMenu = OptionMenu(mainframe, tkvar2, *choices_rw)
#Label(mainframe, text="Select a width").grid(row=1, column=2)
#popupMenu.grid(row=2, column=2)

quit_button = Button(mainframe, text="OK", command=root.destroy).grid(row=5, column=2)

root.mainloop()

# this line works fine on my end.
print("numbers = {}".format(tkvar1.get()))#, tkvar2.get()))