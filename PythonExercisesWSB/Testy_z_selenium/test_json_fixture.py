import pytest
import json


@pytest.fixture(scope='function')
def input_data_users_list():

    with open('data.json', 'r') as f:
        data_list = json.load(f)
    return data_list


def test_data_csv(input_data_users_list):

    users_list_green_pick = []

    for user in input_data_users_list:
        if user['pick'] == 'GREEN':
            print(user)
            users_list_green_pick.append(user)

    assert len(users_list_green_pick) == 187