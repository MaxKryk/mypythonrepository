from PythonForWSBProject.functions.main_menu import main_menu
from PythonForWSBProject.classes.Menu import Menu
def one_track_found_by_tag(Menu, chosenTag):
    for Track in Menu.sortedByTag:
        elementToPlay=Track()
        for Track in Menu.tracks:
            for tag in Track.tags:
                if tag==chosenTag:
                    elementToPlay=Track
                    print("One track found: ", elementToPlay.title)
            operationInput=input("What do you want to do? type play to play\ninfo to show info\nmain to return to main menu")
            if operationInput.lower()=="play" or operationInput.lower()=="p":
                elementToPlay.play()
                one_track_found_by_tag(Menu)
            elif operationInput.lower()=="info" or operationInput.lower()=="i":
                elementToPlay.showInfo()
                one_track_found_by_tag(Menu)
            elif operationInput.lower()=="main" or operationInput.lower()=="m":
                Menu.sortedByTag.clear()
                main_menu(Menu)
            else:
                print()
                print("Unknown command, try again")#red
                one_track_found_by_tag(Menu)