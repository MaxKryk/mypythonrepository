#takes as parameter empty cw_options list and fills it with cw found in the output file
import openpyxl
from openpyxl import load_workbook
import logging

from GUI.logging import logger_setup

arr_deletemelater=[]
def fill_cw_options(arr):
    logger_setup()
    log = logging.getLogger("count entries()")
    workbook = load_workbook(filename="activity_to_clarity.xlsx", data_only=True)
    log.info("wb loaded")
    print(workbook.sheetnames)
    read_from_sheet = workbook.get_sheet_by_name("Read_from")
    log.info("sheet marked: " + read_from_sheet.title)
    # #take first row
    counter=0;


    for rowOfCellObject in read_from_sheet.iter_rows(min_row=1,
                                          min_col=15,
                                          max_col=15,
                                          values_only=True,
                                        ):
        for cell in rowOfCellObject:
            log.info(cell)
            counter=counter+1
            arr.append(cell)
    # log.info("Counter value: ")
    # log.info(counter)
    print(arr)
    arr=list(dict.fromkeys(arr))
    return arr

fill_cw_options(arr_deletemelater)
