

def more_tracks_found_by_criterion(self, userInput):
    self.sorted_by_criterion.clear()
    for element in self.tracks:
        #todo tu przekazanie tego, co jest porownywane
        if (element.author.lower()==userInput.lower()):
            self.sorted_by_criterion.append(element)
        else:
            continue;

    print("List of tracks from ", userInput, ":")
    n=1
    for element in self.sorted_by_criterion:
        print(n, ": ", element.title)
        n=n+1
    print("Chose track by number or type MAIN or M to return to main menu")
    decision=input()
    print("check decision", decision)
    if decision.lower() == 'm' or decision.lower=='main':
        print("check entering main")
        from PythonExercisesWSB.PythonForWSBProject.functions.main_menu import main_menu
        main_menu(self)
    else:
        decisionAsInt=int(decision)
        if decisionAsInt in range(1, len(self.sorted_by_criterion)+1):
            print(decisionAsInt)
            elementToPlay=self.sorted_by_criterion[decisionAsInt-1]
            elementToPlay.play()
            more_tracks_found_by_criterion(self, userInput)
        else:
            print()
            print("Unknown command, try again")
            self.sorted_by_criterion.clear()
            more_tracks_found_by_criterion(self, userInput)#red

#TODO odtwarzanie znalezionych utworow