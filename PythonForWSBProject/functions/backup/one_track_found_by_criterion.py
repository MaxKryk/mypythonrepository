

from PythonExercisesWSB.PythonForWSBProject.classes.Menu  import Menu

########FOUND BY CRITERION
def one_track_found_by_criterion(self):
    print("One track found: ")
    for element in self.sorted_by_criterion:
        elementToPlay=element
        print(element.title)
    operationInput=input("What do you want to do? type play to play\ninfo to show info\nmain to return to main menu")
    if operationInput.lower()=="play" or operationInput.lower()=="p":
        elementToPlay.play()
        one_track_found_by_criterion(self)
    elif operationInput.lower()=="info" or operationInput.lower()=="i":
        elementToPlay.showInfo()
        one_track_found_by_criterion(self)
    elif operationInput.lower()=="main" or operationInput.lower()=="m":
        self.sorted_by_criterion.clear()
        from PythonExercisesWSB.PythonForWSBProject.functions.main_menu import main_menu
        main_menu(self)
    else:
        print()
        print("Unknown command, try again")#red
        one_track_found_by_criterion(self)