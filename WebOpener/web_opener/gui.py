from tkinter import *
from tkinter import messagebox

from selenium import webdriver
import os

# filepath=os.path.dirname(__file__)
# print("przed zmiana" + filepath)
# filepath=filepath.replace("\web_opener","")
# print("po" + filepath)
# filepath=filepath.replace("/", "\\")
# filepath=os.path.join(filepath, "chromedriver.exe")
# filepath=filepath.replace("\\", "\\\\")
# print("relfpath:",  filepath)

#chromedriver=filepath


#driver=webdriver.Chrome(chromedriver)
output_strings=[]
urls=[]

def gui():
    output_strings=[]
    urls=[]

    window = Tk()
    window.title("Web opener")
    #window.geometry('250x150')
    #FUNCTIONS
    def execute():
        given_url=url_input.get()
        lbl_display_text.config(text=given_url)
        urls.append(given_url)
        from web_opener.open_page import start_logic
        start_logic(urls)
        url_input.delete(0, END)
    def onClick():
        print("Show info")
        messagebox.showinfo("How does it work", "Enter URL and press Execute\n - this URL will be opened every 30 minutes")
    #gets result from url_input

    #DECLARATIONS
    lbl_title = Label(window, text="WEB OPENER")
    btn_howto=Button(window, text="How does it work?", bg="orange", fg="black", command=onClick)
    lbl_info = Label(window, text="Enter URI!")
    lbl_input = Label(window, text="INPUT")
    url_input=Entry(window, width=20)
    btn_execute=Button(window, text="Execute!", bg="green", fg="white", command=execute)
    lbl_display_text=Label(window, text="")

    #GRID
    lbl_title.grid(column=0, row=0)
    btn_howto.grid(column=0, row=1)
    lbl_info.grid(column=0, row=2)
    url_input.grid(column=0, row=3)
    btn_execute.grid(column=0, row=4)
    lbl_display_text.grid(column=0, row=5)
    window.mainloop()

#gui()