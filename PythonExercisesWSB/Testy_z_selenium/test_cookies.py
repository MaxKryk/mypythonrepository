import pytest
import json
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webdriver import WebDriver

@pytest.fixture(scope="session")
def chromedriver(request):
    driver = webdriver.Chrome(executable_path="webdriver\\chromedriver.exe")

    def chromedriver_teardown():
        driver.close()

    request.addfinalizer(chromedriver_teardown)
    return driver


@pytest.fixture(scope="session")
def driver(chromedriver):
    """
    :type chromedriver: WebDriver
    """
    chromedriver.get("http://demo.prestashop.com/")
    chromedriver.switch_to.frame(0)
    return chromedriver

def export_cookies_json(cookies_list, file_name="ciasteczka.json"):
    with open(file_name, "w") as file:
        json.dump(cookies_list, file)

def import_cookies_list(file_name="ciasteczka.json"):
    with open(file_name, "r") as file:
        cookies_list = json.load(file)
    return cookies_list

def apply_cookies(driver, cookies_list):
    for cookie in cookies_list:
        driver.add_cookie(cookie)

def test_cookies(driver):
    cookies = driver.get_cookies()

    export_cookies_json(cookies)

    cookies_list = import_cookies_list()
    apply_cookies(driver, cookies_list)