import logging
from GUI.logging import logger_setup
def transform_to_clarity_convention_for_inc_ana_wad(array_to_transform):
    logger_setup()
    log=logging.getLogger("solved_inc_to_inc_ana_wad")
    element=str(array_to_transform)

    #TODO NAMING CONVENTION
    string_constructed_for_clarity=""
    array_template_for_inc=["[","","]{","",";","","}\n"]
    if array_template_for_inc[1] != None:
        array_template_for_inc[1]=str(array_to_transform[0])#day
    if array_template_for_inc[3] != None:
        array_template_for_inc[3]=str(array_to_transform[1])#ticket_id
    if array_template_for_inc[5] == None:
        pass
    else:
        array_template_for_inc[5]=str(array_to_transform[2]).replace(',','.')#time
    log.info(array_template_for_inc)
    for el in array_template_for_inc:
        print(el)
        string_constructed_for_clarity=string_constructed_for_clarity+str(el)
    #element=string_to_construct_for_comment+str(element)
    log.info("string_constructed_for_clarity: ", string_constructed_for_clarity)
    log.info("written to file ", element)

    return string_constructed_for_clarity
