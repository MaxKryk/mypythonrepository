import pytest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.remote.webdriver import WebDriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains


@pytest.fixture(scope="session")
def chromedriver(request):
    driver = webdriver.Chrome(executable_path="webdriver\\chromedriver.exe")

    def chromedriver_teardown():
        driver.close()

    request.addfinalizer(chromedriver_teardown)
    return driver


@pytest.fixture(scope="session")
def driver(chromedriver):
    """
    :type chromedriver: WebDriver
    """
    # chromedriver.get("https://www.seleniumeasy.com/test/drag-and-drop-demo.html")
    chromedriver.get("http://demo.guru99.com/test/drag_drop.html")
    return chromedriver


def test_drag_drop(driver):
    """
    :type driver: WebDriver
    """
    element = driver.find_element_by_xpath("//*[@id='credit2']/a")
    # element = driver.find_element_by_id('credit2').find_element_by_css_selector('a')
    target = driver.find_element_by_id("bank")

    time.sleep(10)

    action_chains = ActionChains(driver)
    action_chains.drag_and_drop(element, target).perform()

    time.sleep(10)