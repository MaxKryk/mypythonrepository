"""Plik zawiera klasę obrabiającą podaną funkcję"""

def f(x):
    return x

class FunctionOperations:
    """Klasa z operacjami na zadanej funkcji f(x). Jako argument konstruktora przyjmuje funkcję z jednym argumentem,
    będącym liczbą float.
    Przykład użycia:

    def funkcja_dwa_x(x):
        return 2*x

    dwa_x = FunctionOperations(funkcja_dwa_x)
    """



    def __init__(self, func):
        if callable(func):
            self.func = func
        else:
            raise ValueError("W konstruktorze podano argument ktory nie jest funckja!!!")

    def __call__(self, x):
        """Oblicz wartość funkcji w punkcie x"""
        return self.func(x)

    def integral(self, value_from, value_to, chunks=1000):
        """Wykonuje najprostsza mozliwa, malo dokładną wersję całki numerycznej.
        Przykład użycia dla obiektu dwa_x:

        wynik = dwa_x.integral(0, 5, chunks=2000)
        wynik powinien wynosic około 25 (pole trojkata prostokatnego o podstawie 5 i wysokosci 10)
        """
        if value_to <= value_from:
            raise ValueError("Zle podany zakres calkowania!")
        step = (value_to - value_from) / chunks
        integral_result = 0
        for i in range(chunks):
            current_x = value_from + step*i
            integral_result += self.func(current_x) * step
        return integral_result

    def derivative(self, value_point, delta=0.001):
        """Liczy wartosc pochodnej funkcji w zadanym punkcie.
        Przykład użycia dla obiektu dwa_x:

        wynik = dwa_x.derivative(2)
        wynik powinien wynosic około 2 (współczynnik wzrostu w dowolnym punkcie funkcji 2x wynosi 2)
        """
        increase_in_delta_range = self.func(value_point + 0.5*delta) - self.func(value_point - 0.5*delta)
        return increase_in_delta_range/delta
