from GUI.append_to_lists import append_to_lists
from GUI.check_wos import check_wos
from GUI.convert_array_with_sorted_entries_to_output_file import create_ea, create_activity, \
    create_change_request_problem
from GUI.convert_excel_to_all_entries_array import convert_excel_to_all_entries_array
from GUI.logging import logger_setup
import logging

def main_logic(kw_num):
    #TODO: SORT
    print("kw_num at the beginning of main logic ", kw_num)
    ###########1

    #deklaracja pustej all_entries_array
    from GUI.solved_inc_to_inc_ana_wad import solved_inc_to_inc_ana_wad

    logger_setup()
    log = logging.getLogger("main logic logger")
    all_entries_array=[]

    #convert_excel_to_all_entries_array
    #in: empty all_entries_array
    #out: filled all_entries_array

    convert_excel_to_all_entries_array(all_entries_array, kw_num)
    log.info("print all_entries_array")
    for element in all_entries_array:
        log.info("csz ", element)
    log.info("all_entries_array has been printed")

    ###########2

    #ACCOUNTS
    #GROUP 1
    ea=[]
    #GROUP 2
    activity=[]
    #GROUP 3 inc_ana_wos_env []{51293257; 1}
    solved_to_process=[]
    inc_ana_wad_to_process=[]
    #TODO igw
    #GROUP 4
    change=[]
    request=[]
    problem=[]
    #GROUP 5
    igw=[]

    lists=[all_entries_array, ea, activity, solved_to_process, change, request, problem]
 #TODO TU BLAD
    append_to_lists(all_entries_array, ea, activity, solved_to_process, problem, change, request)

    for list in lists:
        for element in list:
            log.info(element)
            log.info("next element")
        log.info("next list")

    # ###########3
    # #BY NOW YOU HAVE ALL FILLET LISTS FROM ABOVE
    def create_if_not_empty(arr, method):
        if len(arr) != 0:
            method(arr)

    create_if_not_empty(ea, create_ea)
    create_if_not_empty(activity, create_activity)
    # #create inc
    log.info("Print solved list")
    for element in solved_to_process:
        log.info("stp: ", element)
    log.info("Print inc-ana-wad")
    check_wos(solved_to_process, inc_ana_wad_to_process, igw)


    solved_inc_to_inc_ana_wad(solved_to_process, inc_ana_wad_to_process, igw)

    #cr change
    log.info("change to convert in array")
    for element in change:
        log.info("change element: " + str(element))

    if len(change)==0:
        pass
    else:
        create_change_request_problem(change, 'C#')

    #cr request
    if len(request)==0:
        pass
    else:
        create_change_request_problem(request, 'R#')
    #cr problem
    if len(problem)==0:
        pass
    else:
        create_change_request_problem(problem, 'P#')


main_logic(3)

