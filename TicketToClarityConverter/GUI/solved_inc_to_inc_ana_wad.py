from GUI.check_wos import check_wos
from GUI.convert_array_with_sorted_entries_to_output_file import create_inc_ana_wad_igw
from GUI.logging import logger_setup
import logging
#IN
#filled solved_to_process
#
def solved_inc_to_inc_ana_wad(solved_to_process_arr, inc_ana_wad_to_process_arr, igw_arr):
    logger_setup()
    log=logging.getLogger("solved_inc_to_inc_ana_wad")
    log.info("check content of inc_ana_wad_to_process_arr")
    for element in inc_ana_wad_to_process_arr:
        print("inctp ", element)
    log.info("++++++++++++++")
    #check_wos(solved_to_process_arr, inc_ana_wad_to_process_arr) #TODO SPRAWDZ LISTY
    log.info("check inc ana wad to process after check_wos")
    for element in inc_ana_wad_to_process_arr:
        log.info("inctp2 ", element)
        log.info("========") #WORKS1
    create_inc_ana_wad_igw(inc_ana_wad_to_process_arr, "PRU", False) #todo dziala, mozna odkomentowac jak wau bedzie dzialac
    create_inc_ana_wad_igw(inc_ana_wad_to_process_arr, "WAU", False)
    create_inc_ana_wad_igw(inc_ana_wad_to_process_arr, "RTU", False)
    create_inc_ana_wad_igw(inc_ana_wad_to_process_arr, "INU", False)
    create_inc_ana_wad_igw(igw_arr, "", True)