# from PythonExercisesWSB.PythonForWSBProject.functions.functions import returnTrackIfTheTagMatches
# from PythonExercisesWSB.PythonForWSBProject.functions.functions import findByAuthorInList
# from PythonExercisesWSB.PythonForWSBProject.functions.functions import findByTitleInList
# from PythonExercisesWSB.PythonForWSBProject.functions.functions import findByTagsInList

from PythonForWSBProject.functions.main_menu import findByTagsInList
from PythonForWSBProject.classes.Track import Track


audio01= Track()
audio02=Track()
audio03=Track()

audio01._init_("audio01.mp3", "kalimba1", "Artist2", ["tag1"], "this is a describtion")
audio02._init_("audio02.mp3", "Sleep Away", "Artist2", ["tag1", "tag2"], "this is a describtion")
audio03._init_("audio03.mp3", "Maid with the Flaxen Hair", "Artist1", ["tag1", "tag2", "tag3"], "this is a describtion")
#showTags(kalimba)
#kalimba.play()

tracks = [audio01, audio02, audio03]

#findByTagsInList(tracks, "tag1").play()
foundByTags=[]
print("tag1")

for element in (findByTagsInList(tracks, "tag1", foundByTags)):
    print(element.title)

print("tag2")
for element in (findByTagsInList(tracks, "tag2", foundByTags)):
    print(element.title)
print("tag3")
for element in (findByTagsInList(tracks, "tag3", foundByTags)):
    print(element.title)
