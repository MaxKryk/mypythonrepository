import tkinter as tk

options = [
    "option1",
    "option2",
    "option3",
    "option4"
]

app = tk.Tk()

app.geometry('100x200')

variable = tk.StringVar(app)
variable.set(options[0])

opt = tk.OptionMenu(app, variable, *options)
opt.config(width=90, font=('Helvetica', 12))
opt.pack()

app.mainloop()