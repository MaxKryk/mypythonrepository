import logging
from GUI.logging import logger_setup

def erase_file():
    logger_setup()
    log = logging.getLogger("erase file()")
    #erase content of the file
    log.info("erase content of entries_output.txt")
    filehandle =open("entries_output.txt", "w")
    filehandle.close()
    log.info("content of entries_output.txt erased")


