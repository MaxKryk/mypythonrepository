import logging


#in: array with "Solved" in first column, empty arr_inc_ana_wad to fill
#out: filled arr_inc_ana_wad
#out #TODO filled igw and agw
from GUI.logging import logger_setup


def check_wos(arr_all_solved_to_process, arr_inc_ana_wad, arr_igw):
    logger_setup()
    log = logging.getLogger("check_wos()")
    log.info("len of array solved to process ", len(arr_all_solved_to_process))
    log.info("types ", type(0), type(len(arr_all_solved_to_process)-1))
    log.info("step 1")
    for x in range(0, len(arr_all_solved_to_process)):
        log.info("step 1.1")
        log.info(arr_all_solved_to_process[0])
        log.info('Iteration number: ', x)
        log.info("wos")
        log.info(arr_all_solved_to_process[x][6])
        log.info("step 2")
        if arr_all_solved_to_process[x][6] is None:
            log.info("No wos given")
        else:
            if "WAD" in arr_all_solved_to_process[x][6]:
                log.info(arr_all_solved_to_process[x][6], " appended to arr_inc_ana_wad")
                arr_inc_ana_wad.append(arr_all_solved_to_process[x])
            elif 'NR' in arr_all_solved_to_process[x][6]:
                arr_inc_ana_wad.append(arr_all_solved_to_process[x])
            elif 'Duplicate' in str(arr_all_solved_to_process[x][6]):
                arr_inc_ana_wad.append(arr_all_solved_to_process[x])
            elif 'IGW' in str(arr_all_solved_to_process[x][6]):
                arr_igw.append(arr_all_solved_to_process[x])
            else:
                log.info("no match")
                pass
            log.info("Going to next iteration")
    log.info("CHECKING WOS ENDED, PRINTING arr_inc_ana_wad content:")
    for element in arr_inc_ana_wad:
        log.info(element)
