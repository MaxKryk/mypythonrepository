"""
author: michal.szajkowski@nokia.com
This program downloads PLN exchange rates downloaded from NBP website.
"""

import re
import requests
from json import JSONDecodeError
from datetime import date


class NBPRatesDownloader:
    """Klasa do ściągania kursów"""
    base_url = "http://api.nbp.pl/api/exchangerates/"

    def __init__(self, currency=None):
        self.currency = currency

    def get_rate(self, currency=None, start_date=None, end_date=None):
        if currency is not None:
            self.currency = currency
        rates = list()
        rates_from_api = self._get_rates_dict(self.currency, start_date, end_date)
        for rate_object in rates_from_api:
            rate = str(rate_object["mid"]).replace(".", ",")
            rate_date = rate_object["effectiveDate"]
            rate_no = int(rate_object["no"].split("/")[0])
            rates.append([rate_date, str(rate), str(rate_no)])
        return rates

    def get_available_currencies(self):
        """
        :return: lista dostępnych walut na stronie NBP
        """
        url = self._get_currencies_api_url()
        json_response = self._get_api_json_response(url)
        currencies = [currency["code"] for currency in json_response[0]["rates"]]
        return currencies

    def _get_rates_dict(self, currency, start_date=None, end_date=None):
        url = self._get_rate_api_url(currency, start_date, end_date)
        json_response = self._get_api_json_response(url)
        return json_response["rates"]

    @staticmethod
    def _get_api_json_response(url):
        api_response = requests.get(url)
        try:
            json_response = api_response.json()
        except JSONDecodeError:
            raise ValueError(f"Waluta prawdopodobnie nie istnieje na liscie NBP.\n"
                             f"Odpowiedz NBP: {api_response.text}")
        return json_response

    def _get_rate_api_url(self, currency, start_date=None, end_date=None):
        if start_date is None and end_date is None:
            return self.base_url + "Rates/A/" + currency + "/last"
        else:
            start_date = self._convert_date_to_api_format(start_date)
            end_date = self._convert_date_to_api_format(end_date)
            return '/'.join([self.base_url, "Rates/A", currency, start_date, end_date])

    def _get_currencies_api_url(self):
        return self.base_url + "tables/A/last/"

    @staticmethod
    def _convert_date_to_api_format(date_string):
        if not date_string:
            nice_date = date.today().strftime("%Y-%m-%d")
        else:
            try:
                date_list = re.findall(r"\d+", date_string)
                date_list.reverse()
                nice_date = date(*[int(elem) for elem in date_list]).strftime("%Y-%m-%d")
            except (ValueError, TypeError) as err:
                raise ValueError(f"Podano nieprawidlowa date ({date_string})\n"
                                 f"Prawidlowy format, to DD/MM/YYYY\n"
                                 f"{err}")
        return nice_date