setup(
    name="ticket-to-clarity-converter",
    version="1.0.0",
    description="converts tickets to clarity",
    long_description=README,
    long_description_content_type="text/markdown",

    author="Max Kryk",
    author_email="maximilian.kryk@capgemini.com",
    packages=["GUI"],
    include_package_data=True,
    entry_points={"console_scripts": ["realpython=reader.__main__:gui_new"]},
)