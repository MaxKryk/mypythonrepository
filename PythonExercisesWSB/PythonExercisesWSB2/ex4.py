#2. stwirzyc funkcje z 2 arg na wejsciu, gdzie 1 to tekst, 2 argument bool True jako domyslny - jesli flaga
#jest ustawiona na bool to zwracamy string duzych liter, jak nie to malych
def twoMaximalElements(a, b, c):
    list = [a, b, c]
    list.sort()
    max1=list[len(list)-2]
    max2=list[len(list)-1]
    return max2

def functionReturnString(text, boolean=True):
    if (boolean == True):
        return text.upper()
    elif (boolean == False):
        return text

print(functionReturnString("hello", True))
print(functionReturnString("hello"))
print(functionReturnString("hello", False))